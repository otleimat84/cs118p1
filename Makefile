all_files = 404844706.tar.gz Makefile RDTP.h RDTP.cpp constants.hpp README report.pdf 404.html

.SILENT:



default: RDTP.cpp RDTP.o RDTP.h
	g++ -o mm RDTP.o RDTP.cpp 
clean:
	rm -f server 404844706.tar.gz

dist:
	tar -czvf $(all_files)
