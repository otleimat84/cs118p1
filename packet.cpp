#include <iostream>
#include <stdint.h>
#include <sys/types.h>
#include <strings.h>
#include "constants.h"

//In Spec default values:
#define MAX_PACKET_LENGTH 1024 //bytes
#define MAX_SEQ_NUM 30720 //bytes
#define WND 5120 //bytes


#define BYTEBITMASK 0xFF
#define TWOBYTEBITMASK 0xFFFF
#define HEADER_SIZE 10
#define PACKETSIZE 576
#define MAXDATASIZE 560
#define SYNBITMASK 0b010
#define ACKBITMASK 0b100
#define SYNACKBITMASK 0b110
#define FINBITMASK 0b001

//HEADER INCLUDEES THE FOLLOWING
//32-bit sequence number
//32-bit ACK number
//3-bit Flag field (ACK, SYN, FIN)
//16-bit WindowSize (constant)
//8-bit Checksum
using namespace Values;

class Packet{
public:

  //Constrcutors
  Packet( void ) {}
  Packet( int seq_num, int ack_num, PACKET_TYPE type,
    uint8_t data [], int data_size);
  Packet( uint8_t raw []);
  
  //Public Functions
  PACKET_TYPE GetType      ( void ) { return _type; }
  uint32_t    GetSeq       ( void ) { return _seq; }
  uint32_t    GetAck       ( void ) { return _ack; }
  bool        IsValid      ( void ) { return VerifyChecksum(); }
  void        GetRawData   ( uint8_t* raw );

private:
  //Helper Functions
  void    SetRawData     ( uint8_t *raw );
  uint8_t CreateChecksum ( void );
  bool    VerifyChecksum ( void );

  //Private member variables
  uint8_t      _raw_data [PACKETSIZE];
  uint16_t     _seq;
  uint16_t     _ack;
  uint16_t     _wnd;
  uint16_t     _size;
  PACKET_TYPE  _type;
  uint8_t      _checksum;
};

uint8_t Packet::CreateChecksum( void ){
  int checksum = 0;
  int size = _size;
  
  for (int i = 0; i < size; i++){
    checksum += _raw_data[i];
    if (checksum & 0b100000000) //If overflow bit is set
      checksum++;
    checksum = (checksum & BYTEBITMASK);
  }

  checksum = ~checksum;

  return checksum;
}

bool Packet::VerifyChecksum( void ){
  int checksum = 0;
  int size = _size;

  for (int i = 0; i < size; i++){
    checksum += _raw_data[i];
    if (checksum & 0b100000000) //If overflow bit is set
      checksum++;
    checksum = (checksum & BYTEBITMASK);
  }

  if (checksum == 0b11111111)
    return true;

  return false;
}

void Packet::GetRawData ( uint8_t *raw ){
  for (int i = 0; i < _size; i++)
    raw[i] = _raw_data[i];
}

void Packet::SetRawData ( uint8_t *raw ){
  for (int i = 0; i < _size; i++)
    _raw_data[i] = raw[i];
}

//Constructor used for RECIEVING raw data from another host
Packet::Packet(uint8_t raw []){
   //Source
 
  _seq = (raw[0] << 8) + raw[1];
  _ack = (raw[2] << 8) + raw[3];
  _wnd = (raw[4] << 8) + raw[5];
  _size = (raw[6] << 8) + raw[7];
  _checksum = raw[9];
  
  if ((raw[8] & SYNACKBITMASK) == SYNACKBITMASK)
    _type = SYNACK;
  else if ((raw[8] & SYNBITMASK) == SYNBITMASK)
    _type = SYN;
  else if ((raw[8] & ACKBITMASK) == ACKBITMASK)
    _type = ACK;
  else if ((raw[8] & FINBITMASK) == FINBITMASK)
    _type = FIN;
  else
    _type = NONE;

  SetRawData(raw);
  
}

//Constructor used for CREATING new packets for another host
Packet::Packet(int seq_num, int ack_num, PACKET_TYPE type,
         uint8_t data [], int data_size){
  int packet_size = data_size + HEADER_SIZE;

  //Fill in raw data
  bzero(_raw_data, PACKETSIZE);
  
  //Sequence Number
  _raw_data[0] = (seq_num >> 8) & BYTEBITMASK;
  _raw_data[1] =  seq_num       & BYTEBITMASK;

  //Acknowledgement Number
  _raw_data[2] = (ack_num >> 8) & BYTEBITMASK;
  _raw_data[3] =  ack_num       & BYTEBITMASK;

  //Window Size
  _raw_data[4] = (WND >> 8) & BYTEBITMASK;
  _raw_data[5] =  WND       & BYTEBITMASK;

  //Packet Size
  _raw_data[6] = (packet_size >>  8) & BYTEBITMASK;
  _raw_data[7] =  packet_size        & BYTEBITMASK;

  //Packet Type
  if (type == ACK)
    _raw_data[8] = 0b100;
  else if (type == SYN)
    _raw_data[8] = 0b010;
  else if (type == SYNACK)
    _raw_data[8] = 0b110;
  else if (type == FIN)
    _raw_data[8] = 0b001;
  else
    _raw_data[8] = 0b000;
  

  //PUT IN DATA
  for (int i = 0; i < data_size; i++)
    _raw_data[i + 10] = data[i];
  
  //Calculate Checksum
  _size = packet_size;
  _checksum = CreateChecksum();
  _raw_data[9] =  _checksum;

  //Put in other private member variables
  _seq = seq_num;
  _ack = ack_num;
  _wnd = WND;
  _type = type;


}
/*
int main (void){
  uint8_t hold [PACKETSIZE];
  uint8_t omar [200] = "Hi Omar";
  Packet p(101, 202, SYNACK, NULL, 0);
  p.GetRawData(hold);
  Packet k(hold);

  std::cout << k.IsValid();

  return 0;
}
*/