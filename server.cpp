#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <fstream>
#include <cassert>
#include <sstream>
#include <string>
#include <iterator>
#include <iomanip>

#include "RDTP.h"
#include "FileTransfer.h"

using namespace std;
int sockfd, portno;
struct sockaddr_in serv_addr;
string fileName;
void makeConnection();


int main(int argc, char** argv)
{
    if (argc < 2)
    {
        fprintf(stderr, "See usage\n");
        exit(1);
    }
    portno = atoi(argv[1]);
    RDTP::ConnectionHandler ch = RDTP::ConnectionHandler(sockfd, false);
    FileTransfer transfer;
    while (1){
        transfer = FileTransfer(ch);
    }

    close(sockfd);
    return 0;
}

void makeConnection(){
    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0)
        perror("socket error");
    memset(&serv_addr, '\0', sizeof(serv_addr));
    portno = atoi(argv[1]);
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr.sin_port = htons(portno);
    if (::bind(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
        error("bind error");
}