#include <string>
#include <stdint.h>

#include "RDTP.h"



class FileTransfer
{
public:
    FileTransfer::FileTransfer(RDTP::ConnectionHandler& ch);

   std::string FileTransfer::getFileName();
    void FileTransfer::SendFilename(const std::string& filename);
    void FileTransfer::FileNotFound();
        

    void FileTransfer::TransferFile(std::basic_istream<char>& is, size_t size);
    void FileTransfer::ReceiveFile(std::basic_ostream<char>& os);
private:
    RDTP::ConnectionHandler& m_ch;
};
