#include <vector>
#include <iterator>
#include <stdint.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include "RDTP.h"
#include "constants.h"


using namespace Values;

RDTP::ConnectionHandler::ConnectionHandler(int sockFd, bool client){
	m_sockFd = sockFd;
	m_isClient = client;
	m_cli_len = sizeof(m_cli_addr);
	m_isEstablished = true;
	isClient() ? clientHandshake(): serverHandshake();
}

void RDTP::ConnectionHandler::setTimeout(int sec, int microsec){
	struct timeval tv;
	tv.tv_sec = sec;
	tv.tv_usec = microsec;
	//setsockopt((m_sockFd, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)));
}


bool RDTP::ConnectionHandler::incrementing(Packet packet){
	if (packet.GetType == constants::SYN){
		m_recBase = packet.GetAck() + 1;
		return true;
	}
	return false;
}

void RDTP::ConnectionHandler::clientHandshake(){
	int length;
	uint8_t buffer[MaxPacketSize];
	bool retransmit = false;
	setTimeout(0, RTO_us);
	while (1){
		//write(m_sockFd, packet.GetRawData(), packet.GetRawDataSize());
		length = recvfrom(m_sockFd, buffer, MaxPacketSize, 0, (struct sockaddr*)&m_cli_addr, &m_cli_len);
		if (length <= 0){
			retransmit = true;
			continue;
		}
		Packet packet = Packet(buffer);
		if (incrementing(packet))
			break;

		m_sendBase += 1;
		m_nextSeqNum += 1;
	}
}

void RDTP::ConnectionHandler::serverHandshake(){
	int length;
	uint8_t buffer[MaxPacketSize];
	bool retransmit = false;
	setTimeout(0, 0);
		while (1){
		//write(m_sockfd, packet.GetRawData(), packet.getRawDataSize());
		length = recvfrom(m_sockFd, buffer, MaxPacketSize, 0, (struct sockaddr*)&m_cli_addr, &m_cli_len);
		if (length > 0){			

		Packet packet = Packet(buffer);
		if (incrementing(packet))
			break;
		else {
			if (packet.GetType() == PACKET_TYPE::FIN){
				Packet ack = Packet(0,0, m_nextSeqNum, packet.GetAck(), PACKET_TYPE::ACK);
				//sendto(m_sockFd, ack.GetRawData(), ack.GetRawDataSize(),  0, (struct sockaddr*)&_cli_addr, _cli_len);
				continue;
			}

			}
		}
			

		


	}
	setTimeout(0, RTO_us);
	//Packet p = Packet(0, 0, m_nextSeqNum, 0, nullptr, PACKET_TYPE::SYNACK);
	retransmit = false;
	while (1){
		
	}
	m_sendBase += 1;
	m_nextSeqNum += 1;
}


