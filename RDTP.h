#include <vector>
#include <istream>
#include <ostream>
#include <utility>
#include <stdint.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <list>
#include <queue>
#include <vector>
#include <algorithm>
#include <unistd.h>
#include "packet.cpp"

class RDTP {


	class ConnectionHandler {
	public:
		ConnectionHandler(int sockFd, bool client);
		~ConnectionHandler();
		// NEED TO DO
		void readData();
		void writeData();


	private:
		void clientHandshake();
		void serverHandshake();
		bool incrementing(Packet packet);
		void setTimeout(int sec, int uSec);
		bool m_isClient;
		bool m_isEstablished;
		int m_sendBase, m_recBase;
		int m_nextSeqNum;
		int m_sockFd;
		struct sockaddr_in m_cli_addr;
		socklen_t m_cli_len;
		//Packet* firstPacket = nullptr;
		//std::list<std::pair<Packet, uint64_t>> m_receivedPackets;
		bool isClient(){return m_isClient;}
	};


};